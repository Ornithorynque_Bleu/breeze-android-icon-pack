package org.xphnx.ameixamonochrome.interfaces;

import android.graphics.Bitmap;

public interface BitmapListener {
    void onBitmap(Bitmap bitmap);
}

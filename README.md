
Breeze port for Trebuchet, KISS Launcher, OpenLauncher, Lawnchair Launcher, Adw, and many more launchers.

Code forked from [Ameixa Monochrome](https://gitlab.com/xphnx/ameixa-monochrome).
Only apps hosted in [F-Droid](https://f-droid.org) are supported.

## Screenshots

![Screenshot1](./screenshots/1.jpg)
![Screenshot2](./screenshots/2.jpg)
![Screenshot3](./screenshots/3.jpg)


## Installation
<!--<a href="https://f-droid.org/packages/org.xphnx.ameixamonochrome">
    <img src="https://f-droid.org/badge/get-it-on.png"
         alt="Get it on F-Droid" height="80">
</a>-->
<a href="https://gitlab.com/Ornithorynque_Bleu/breeze-android-icon-pack/blob/master/app/build/outputs/apk/debug/app-debug.apk">
    <img src="https://hike.in/images/hike5.0/apk.png"
         alt="Download apk" height="80">
</a>

## License

<img src="https://www.gnu.org/graphics/gplv3-127x51.png" />

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and other libre or open licenses for the artwork (see [CREDITS](https://gitlab.com/xphnx/twelf_cm12_theme/blob/master/CREDITS.md) for more details)
